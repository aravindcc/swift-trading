#!/bin/bash
cd Core
./update.sh "$1"
cd HarmonicCore 
git commit -am "update build: $1"
cd ../../HarmonicServer
swift package update
cd ../TradingServer
swift package update
cd ..