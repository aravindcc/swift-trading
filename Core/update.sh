#!/bin/bash
cd TradingCore
git add . 
git commit -am "$1"
cd ..
cd HarmonicCore
swift package update
cd ..
cd TradingNotify
swift package update TradingCore
cd ..