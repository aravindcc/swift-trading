#!/bin/bash
docker-compose pull
docker-compose kill trader
docker-compose kill analyser
docker-compose kill light
docker-compose up -d postgres
docker-compose up -d light
docker-compose up -d trader
docker-compose up -d analyser
docker-compose logs -f