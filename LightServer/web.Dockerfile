FROM node:10

# Create app directory
WORKDIR /usr/src/app

# Bundle app source
COPY ./LightServer .

# RUN npm install
# If you are building your code for production
RUN npm ci --only=production

EXPOSE 80
CMD [ "node", "index.js" ]