const requirejs = require("requirejs");

requirejs.config({
  deps: ["lightstreamer_node.js"],
  nodeRequire: require
});

requirejs(["LightstreamerClient", "Subscription"], function(
  LightstreamerClient,
  Subscription
) {
  const express = require("express");
  const app = express();
  app.use(express.json());
  const port = 80;
  var lightServer = undefined;
  var priceSub = undefined;
  var waitingRequests = {};

  function addMarket(idInt, res) {
    if (lightServer == undefined) {
      return;
    }

    var id = "PRICE." + idInt;

    lightServer.unsubscribe(priceSub);

    var items = priceSub.getItems();
    var contains = false;
    for (var i = 0; i < items.length; i++) {
      if (items[i] == id) {
        contains = true;
      }
    }

    if (!contains) {
      items.push(id);
      priceSub.setItems(items);
    }

    if (id in waitingRequests) {
      waitingRequests[id].push(res);
    } else {
      waitingRequests[id] = [res];
    }

    console.log("ADDED");
    lightServer.subscribe(priceSub);
  }

  app.get("/audit/:id", function(req, res) {
    var marketID = req.params.id;
    console.log("ADDING: " + marketID);
    addMarket(marketID, res);
  });

  app.post("/connect", function(req, res) {
    if (lightServer == undefined) {
      var url = req.body.url;
      var username = req.body.username;
      var password = req.body.password;
      var adapter = req.body.adapter;

      lightServer = new LightstreamerClient(url, adapter);
      lightServer.connectionDetails.setServerAddress(url);
      lightServer.connectionDetails.setUser(username);
      lightServer.connectionDetails.setPassword(password);
      lightServer.connectionDetails.setAdapterSet(adapter);

      priceSub = new Subscription("MERGE", [], ["AuditId", "Bid", "Offer"]);
      priceSub.setDataAdapter("PRICES");
      priceSub.setRequestedSnapshot("yes");

      priceSub.addListener({
        onSubscription: function() {
          console.log("SUBSCRIBED");
        },
        onUnsubscription: function() {
          console.log("UNSUBSCRIBED");
        },
        onItemUpdate: function(obj) {
          var name = obj.getItemName();
          var audit = obj.getValue("AuditId");
          var bid = parseFloat(obj.getValue("Bid"));
          var offer = parseFloat(obj.getValue("Offer"));
          var resp = {
            AuditId: audit,
            Bid: bid,
            Offer: offer
          };
          if (name in waitingRequests) {
            for (var i = 0; i < waitingRequests[name].length; i++) {
              waitingRequests[name][i].json(resp);
            }
            delete waitingRequests[name];
            lightServer.unsubscribe(priceSub);
            priceSub.setItems(Object.keys(waitingRequests));
            lightServer.subscribe(priceSub);
          }
        }
          .bind(waitingRequests)
          .bind(lightServer)
          .bind(priceSub)
      });

      lightServer.addListener({
        onStatusChange: function(newStatus) {
          if (
            (newStatus == "CONNECTED:HTTP-STREAMING" ||
              newStatus == "CONNECTED:WS-STREAMING") &&
            res != undefined
          ) {
            res.send("SUCCESS");
            res = undefined;
          } else if (newStatus == "DISCONNECTED" && res != undefined) {
            lightServer = undefined;
            priceSub = undefined;
            res.status(400).send({
              message: "This is an error!"
            });
            res = undefined;
          }
        }.bind(res)
      });

      lightServer.connect();
      lightServer.subscribe(priceSub);
    } else {
      res.send("SUCCESS");
    }
  });

  app.listen(port, () =>
    console.log(`Example app listening at http://localhost:${port}`)
  );
});
