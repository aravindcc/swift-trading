#!/bin/bash

if [ -z "$1" ] || [ $1 = "analyse" ] ; then
docker build -t eu.gcr.io/lunar-linker-273201/forex-analyse -f ./HarmonicServer/web.Dockerfile .
docker push eu.gcr.io/lunar-linker-273201/forex-analyse
fi

if [ -z "$1" ] || [ $1 = "trade" ] ; then
docker build -t eu.gcr.io/lunar-linker-273201/forex-trade -f ./TradingServer/web.Dockerfile .
docker push eu.gcr.io/lunar-linker-273201/forex-trade
fi

if [ -z "$1" ] || [ $1 = "light" ] ; then
docker build -t eu.gcr.io/lunar-linker-273201/forex-lightstream -f ./LightServer/web.Dockerfile .
docker push eu.gcr.io/lunar-linker-273201/forex-lightstream
fi

if [ $1 = "backtester" ] ; then
docker build --no-cache -t eu.gcr.io/lunar-linker-273201/forex-backtester -f ./TradingBacktester/web.Dockerfile .
docker run -dit eu.gcr.io/lunar-linker-273201/forex-backtester
BACKTEST_BUILD=$(docker ps -aqf "ancestor=eu.gcr.io/lunar-linker-273201/forex-backtester" -f "status=running")
docker cp "${BACKTEST_BUILD}:/build/bin/Run" CloudTester/
docker kill "${BACKTEST_BUILD}"
fi

if [ -z "$1" ] ; then
scp -r CloudDefault aravind_dharmalingam@35.189.114.114:/home/aravind_dharmalingam/
ssh aravind_dharmalingam@35.189.114.114  "cd CloudDefault; bash redeploy.sh"
fi

if [ $1 = "analyse" ] ; then
scp -r CloudDefault aravind_dharmalingam@35.189.114.114:/home/aravind_dharmalingam/
ssh aravind_dharmalingam@35.189.114.114  "cd CloudDefault; bash reanalyse.sh"
fi

if [ $1 = "backtester" ] ; then
ssh aravind_dharmalingam@35.189.114.114  "pkill Run; rm -rf CloudTester;"
scp -r CloudTester aravind_dharmalingam@35.189.114.114:/home/aravind_dharmalingam/
ssh aravind_dharmalingam@35.189.114.114  "cd CloudTester; mkdir insight; export FX_DOCKER=true; ./Run > progress 2>&1 &"
ssh aravind_dharmalingam@35.189.114.114  "tail -f CloudTester/progress"
fi

if [ $1 = "backtester-test" ] ; then
docker build --no-cache -t eu.gcr.io/lunar-linker-273201/forex-backtester-test -f ./TradingBacktester/test.Dockerfile .
docker run --env FX_DOCKER=true eu.gcr.io/lunar-linker-273201/forex-backtester-test
fi

if [ $1 = "backtester-test-remote" ] ; then
docker build --no-cache -t eu.gcr.io/lunar-linker-273201/forex-backtester-test -f ./TradingBacktester/test.Dockerfile .
docker push eu.gcr.io/lunar-linker-273201/forex-backtester-test
ssh aravind_dharmalingam@35.189.114.114  "docker kill $(docker ps -aqf status=running)"
ssh aravind_dharmalingam@35.189.114.114 "docker run -d --name backtester --env FX_DOCKER=true eu.gcr.io/lunar-linker-273201/forex-backtester-test"
ssh aravind_dharmalingam@35.189.114.114 "docker logs -f backtester"
fi
